from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Jadwal
from .forms import Jadwals

# Create your views here.
response = {}
def homepage(request):
    response['activetab'] = 'homepage'
    return render(request,'app1/homepage.html',response)

def pendidikan(request):
    response['activetab'] = 'pendidikan'
    return render(request,'app1/pendidikan.html',response)

def pengalaman(request):
    response['activetab'] = 'pengalaman'
    return render(request,'app1/pengalaman.html',response)

def proyek(request):
    response['activetab'] = 'proyek'
    return render(request,'app1/proyek.html',response)

def jadwal(request):
    response['activetab'] = 'jadwal'
    return render(request,'app1/jadwal.html', response)

def formJadwal(request):
    if(request.method == 'POST'):
        form = Jadwals(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            Jadwal.objects.create(**data)
            return HttpResponseRedirect('/daftarKegiatan')
    else:
        form = Jadwals()
    response['form'] = form
    return render(request, 'app1/formJadwal.html', response)

def daftarKegiatan(request):
    if(request.method == 'POST'):
        Jadwal.objects.all().delete()
        return HttpResponseRedirect('/daftarKegiatan')
    listAktivitas = Jadwal.objects.all().order_by("-id")
    response['aktivitas'] = listAktivitas
    return render(request, 'app1/daftarKegiatan.html', response)
