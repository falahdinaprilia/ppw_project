from django.db import models
from django.utils import timezone
from datetime import date, time

# Create your models here.
class Jadwal(models.Model):
    namaKegiatan = models.CharField(max_length = 50)
    tanggal = models.DateField()
    waktu = models.TimeField()
    tempat = models.CharField(max_length = 50)
    kategori = models.CharField(max_length = 50)