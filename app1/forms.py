from django import forms

class Jadwals(forms.Form):
    namaKegiatan = forms.CharField(label='Nama Kegiatan', required=True,
        max_length=50, widget=forms.TextInput())
    tanggal = forms.DateField(label='Tanggal', required=True,
        widget=forms.DateInput(attrs={'type': 'date'}))
    waktu = forms.TimeField(label='Waktu', required=True,
        widget=forms.TimeInput(attrs={'type': 'time'}))
    tempat = forms.CharField(label='Tempat', required=True,
        max_length=50, widget=forms.TextInput())
    kategori = forms.CharField(label='Kategori', required=True,
        max_length=50, widget=forms.TextInput())
